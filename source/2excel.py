# Example of python program for KrotW32 zoom data analyzing in MS Excel.
# (C) 2018 by Vitaly Bogomolov mail@vitaly-bogomolov.ru
import os
import tempfile
import json
import csv


def proc_input(input_file_name):
    arr = json.loads(open(input_file_name, 'rt').read())["dataRow"]
    output_file = tempfile.NamedTemporaryFile(delete=False)
    output_file_name = output_file.name
    writer = csv.writer(output_file, delimiter=';')
    for row in arr:
        writer.writerow(row)
    output_file.close()
    csv_file = "{}.csv".format(output_file_name)
    os.rename(output_file_name, csv_file)
    print csv_file
    # call excel
    # os.system('start "{}"'.format(csv_file))
    os.system(csv_file)


if __name__ == "__main__":  # pragma: no cover
    import sys
    if len(sys.argv) > 1:
        proc_input(sys.argv[1])
    else:
        print "Usage: 2excel datafile.json"
