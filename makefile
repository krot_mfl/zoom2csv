.PHONY: all release

ifeq ($(OS),Windows_NT)
include windows.mk
else
include linux.mk
endif

all: release

release:
	@$(PYTHON) setup.py py2exe
